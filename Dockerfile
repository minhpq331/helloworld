FROM node:alpine

MAINTAINER minhpq331@gmail.com

WORKDIR /app

ADD package.json yarn.lock /app/
RUN yarn --pure-lockfile

ADD . /app

CMD ["node", "index.js"]